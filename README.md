# IR Mini-Project: Knowledge Graph Fusion

This repository contains the current status of the work on the mini-project for the course IR at the UPB in SS 18 of the group Viribus Unitis. 
The mini-project deals with knowledge graph fusion. More precisely the goal is to fuse different versions (language wise) of DBpedia into a single large knowledge graph.

## Approach
For an explanation of the approach we follow, we refer the interested reader to our [Final Presentation](https://bitbucket.org/viribusunitis/ir_miniproject/src/master/IR_FinalPresentation_ViribusUnitis.pdf).

## Prerequesites
For using most parts of our tool, we require a working [Apache Jena TDB](https://jena.apache.org/documentation/tdb/) storage containing the DBpedia knowledge graphs (or a subset of each of them) to work with. For our evaluation we used the following DBpedia languages. 

* [German](http://downloads.dbpedia.org/2016-10/core-i18n/de/)
* [Dutch](http://downloads.dbpedia.org/2016-10/core-i18n/nl/)
* [Portuguese](http://downloads.dbpedia.org/2016-10/core-i18n/pt/)

The links above lead to the latest DBpedia dump (of the corresponding language) from October 2016, which we are using. In order to setup the TDB storage, you have to download all of the files of the dump which define the subset of the knowledge graph you are interested in. For our evaluation we used the following files (where the * needs to be replaced by the according language tag).

* category_labels_*.ttl.bz2
* instance_types_*.ttl.bz2
* labels_*.ttl.bz2
* long_abstracts_*.ttl.bz2
* mappingbased_literatls_*.ttl.bz2
* mappingbased_objects_*.ttl.bz2
* pnd_*.ttl.bz2 (sometimes called persondata_)

The files for one language should be in a directory titled with the abbreviation of the language (de, nl or pt in our case). Additionally the ontology file dbpedia_2016-10.owl needs to be in the directory containing the language directory. Thus, your directory structure should look like this: 

* folder_name
    * dbpedia_2016-10.owl
    * de
        * category_labels_de.ttl.bz2
        * ...
    * pt
        * category_labels_pt.ttl.bz2
        * ...
    * nl
        * category_labels_nl.ttl.bz2
        * ...

Now you can initiate a TDB storage creation by running the [`RunTDBStorageCreation`](https://bitbucket.org/viribusunitis/ir_miniproject/src/master/kg-fusion/src/main/java/de/upb/vu/kgf/query/tdb/RunTDBStorageCreation.java) class with two parameters: 

1. Path to the directory where the TDB storage should be created
2. Path to the folder containing the DBpedia dump (e.g. `C:/path/to/folder_name` in the example above)

When using the files suggested above, the import roughly takes 45 minutes, which is always depending on your machine and any parallel IO tasks.

## Building and Running

### Building
As the engine is a simple Maven project, it can be built by running

``
mvn package
``

once the user navigated into the directory of the project.

The pom is configured in such a way to generate a single runnable jar containing all dependencies in the target directory of the project featuring the name 

``
kg-fusion-0.0.1-SNAPSHOT.jar
``

### Running an Evaluation

In order to run an evaluation, first download both the [evaluation data](https://bitbucket.org/viribusunitis/ir_miniproject/src/master/kg-fusion/src/main/resources/evaluation_data/) and the [input dataset](https://bitbucket.org/viribusunitis/ir_miniproject/src/master/kg-fusion/src/main/resources/evaluation_data/persons.txt). 
The latter is a simple .txt file containing the IRIs from the German DBpedia dump of the entities which should be merged in order to evaluate the approach. The first one is a directory `evaluation_data` containing a file `persons.txt` and a folder `entity`, which contains one file per entity defining its gold standard after fusion. If these resources are present (and the TDB storage is setup), you can run an evaluation with the following command: 

``
java -jar kg-fusion-0.0.1-SNAPSHOT.jar path/to/tdb_storage_dir path/to/input_dataset_file path/to/evaluation_data/entity/
``

Such an evaluation takes roughly 3 minutes depending on your machine. The result of the evaluation is printed into a file `eval_dd-MM-yyy_hh:mm:ss` in a folder called `evaluation` next to the jar, which is created incase it does not exist. Furthermore the results will be printed to the console. Additionally the resulting knowledge graph is written to files in a folder `results/entity` next to the jar. The structure of the files is the same es for the evaluation entities (one file per entity).
