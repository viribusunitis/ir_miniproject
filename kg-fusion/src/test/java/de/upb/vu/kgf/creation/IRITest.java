package de.upb.vu.kgf.creation;


import static org.junit.Assert.assertEquals;

import org.junit.Test;

import de.upb.vu.kgf.datastructure.IRI;


public class IRITest {

   @Test
   public void testPrefixedIri() {
      String rawPrefixedIri = "dbo:type";
      IRI iri = new IRI(rawPrefixedIri);
      IRI expectedIRI = new IRI("http://dbpedia.org/ontology/type");
      assertEquals(expectedIRI, iri);
   }


   @Test
   public void testNonPrefixedIri() {
      String rawPrefixedIri = "http://dbpedia.org/ontology/type";
      IRI iri = new IRI(rawPrefixedIri);
      assertEquals(rawPrefixedIri, iri.getIdentifier());
   }
}
