package de.upb.vu.kgf.playground;


import java.io.FileNotFoundException;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;

import de.upb.vu.kgf.query.executor.SparqlTTLQueryExecuter;


public class SparqlTTLQueryExecutorPlayground {

   private static final String FOLDER_PATH = "src/test/resources/playground/sparql_ttl_query_executor/";


   public static void main(String[] args) throws FileNotFoundException {
      testImport();
   }


   public static void testImport() throws FileNotFoundException {
      SparqlTTLQueryExecuter executer = new SparqlTTLQueryExecuter(FOLDER_PATH);
      String query = "PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" + //
            "PREFIX  dbo: <http://dbpedia.org/ontology/>" + //
            "PREFIX  dbp: <http://dbpedia.org/property/>" + //
            "SELECT (count(*) AS ?count) " + //
            "WHERE{" + //
            "    ?resource  rdf:type  dbo:Person;" + //
            " }";
      ResultSet resultSet = executer.executeQuery(query);
      while (resultSet.hasNext()) {
         QuerySolution solution = resultSet.next();
         System.out.println(1 == solution.getLiteral("?count").getInt());
      }
   }

}
