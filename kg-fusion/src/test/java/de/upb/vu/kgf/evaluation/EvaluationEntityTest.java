package de.upb.vu.kgf.evaluation;


import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.evaluation.entity.EvaluationEntityCreator;
import de.upb.vu.kgf.evaluation.entity.EvaluationEntityReader;
import de.upb.vu.kgf.fusion.EntityWriter;


public class EvaluationEntityTest {

   private static final String FOLDER_PATH = "src/test/resources/evaluation/";


   @Test
   public void testCreator() throws IOException {
      EvaluationEntityCreator.create(FOLDER_PATH);
      List<Entity> entities = EvaluationEntityCreator.getMergedEntities();
      Assert.assertEquals(100, entities.size());
      Assert.assertEquals(55, entities.stream().filter(e -> e.getLabel().equals("Angela Merkel")).findFirst().get().getProperties().size());
   }


   @Test
   public void testReader() throws IOException {
      EvaluationEntityCreator.create(FOLDER_PATH);
      EntityWriter.writeEntitiesToFile(FOLDER_PATH, EvaluationEntityCreator.getMergedEntities());
      EntityWriter.writeIRIsToFile(FOLDER_PATH, EvaluationEntityCreator.getIRIs());

      List<Entity> entities = EvaluationEntityReader.read(FOLDER_PATH + "entity/");
      Assert.assertEquals(100, entities.size());
      Assert.assertEquals(55, entities.stream().filter(e -> e.getLabel().equals("Angela Merkel")).findFirst().get().getProperties().size());
   }

}
