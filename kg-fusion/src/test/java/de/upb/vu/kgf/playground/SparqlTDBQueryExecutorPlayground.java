package de.upb.vu.kgf.playground;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.jena.query.ResultSet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.kgf.Language;
import de.upb.vu.kgf.TDBStorageLocations;
import de.upb.vu.kgf.datageneration.BenchmarkDataGenerator;
import de.upb.vu.kgf.query.executor.SparqlTDBQueryExecutor;


public class SparqlTDBQueryExecutorPlayground {

   private static final Logger LOGGER = LoggerFactory.getLogger(SparqlTDBQueryExecutorPlayground.class);


   public static void getAmountOfPersonsInGermanDataset() {
      SparqlTDBQueryExecutor executer = new SparqlTDBQueryExecutor(TDBStorageLocations.STORAGE_PATH_DE);
      String query = "PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" + //
            "PREFIX  dbo: <http://dbpedia.org/ontology/>" + //
            "PREFIX  dbp: <http://dbpedia.org/property/>" + //
            "SELECT (count(*) AS ?count) " + //
            "WHERE{" + //
            "    ?resource  rdf:type  dbo:Person;" + //
            " }";
      ResultSet resultSet = executer.executeQuery(query);
      resultSet.forEachRemaining(s -> System.out.println(s.toString()));
   }


   public static void getFirst100PersonsInGermanDataset() {
      SparqlTDBQueryExecutor executer = new SparqlTDBQueryExecutor(TDBStorageLocations.STORAGE_PATH_DE);
      String query = "PREFIX  rdf:  <http://www.w3.org/1999/02/22-rdf-syntax-ns#>" + //
            "PREFIX  dbo: <http://dbpedia.org/ontology/>" + //
            "PREFIX  dbp: <http://dbpedia.org/property/>" + //
            "SELECT ?s ?p ?o " + //
            "WHERE{" + //
            "    ?s  rdf:type  dbo:Person ." + //
            "    ?s ?p ?o . " + //
            " }" //
            + "LIMIT 100";
      ResultSet resultSet = executer.executeQuery(query);
      resultSet.forEachRemaining(s -> System.out.println(s.toString()));
   }


   public static void executeTestQuery() throws IOException {
      SparqlTDBQueryExecutor executer = new SparqlTDBQueryExecutor(TDBStorageLocations.STORAGE_PATH_DE);
      String query = de.upb.vu.kgf.util.IOUtils.readStringFromFile("src/test/resources/test_query.txt");
      ResultSet resultSet = executer.executeQuery(query);
      resultSet.forEachRemaining(s -> System.out.println(s.toString()));
   }


   public static void generatePersonDataTest() {
      BenchmarkDataGenerator generator = new BenchmarkDataGenerator(TDBStorageLocations.STORAGE_PATH);
      List<String> names = Arrays.asList("Barack Obama", "Gerhard Schröder", "Chester Bennington", "Roger Moore", "Hans Albert Einstein",
            "Ludwig van Beethoven", "Heinrich Schliemann", "Albert Einstein");
      generator.generateData(names, "Person");
   }


   public static void generatePlaceDataTest() {
      BenchmarkDataGenerator generator = new BenchmarkDataGenerator(TDBStorageLocations.STORAGE_PATH);
      List<String> names = Arrays.asList("Rom", "Paris", "Berlin", "Köln", "Vatikan", "Portugal", "Mars", "New York", "Mount Everest",
            "Japan", "Paderborn", "Australien");
      generator.generateData(names, "Place");
   }


   public static void generateEntitiesExistingInAllDatabases() {
      String entityType = "Person";
      String queryString = "PREFIX  rdf:    <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " //
            + "PREFIX   owl:  <http://www.w3.org/2002/07/owl#> " //
            + "PREFIX  dbo:   <http://dbpedia.org/ontology/> " //
            + "SELECT DISTINCT ?entity WHERE {?entity rdf:type dbo:" + entityType + "}";
      List<String> entities = new ArrayList<>();
      SparqlTDBQueryExecutor executer = new SparqlTDBQueryExecutor(TDBStorageLocations.STORAGE_PATH_DE);
      ResultSet resultSet = executer.executeQuery(queryString);
      resultSet.forEachRemaining(s -> {
         String entityName = s.getResource("?entity").getURI().replaceAll("http://de.dbpedia.org/resource/", "");
         entities.add(entityName);
      });

      LOGGER.info("Starting generation with {} base entities.", entities.size());

      BenchmarkDataGenerator dataGenerator = new BenchmarkDataGenerator(TDBStorageLocations.STORAGE_PATH);
      dataGenerator.generateData(entities, entityType, 100);

      executer.shutdown();
   }


   public static void findEntityDistribution() {
      for (Language language : Language.values()) {
         String storagePath = TDBStorageLocations.STORAGE_PATH + "/" + language.getAbbreviation();
         SparqlTDBQueryExecutor executer = new SparqlTDBQueryExecutor(storagePath);

         System.out.println("Language: " + language.getAbbreviation());

         String[] entityTypes = { "Person", "Work", "Species", "Organisation", "Place" };
         for (String entityType : entityTypes) {
            String queryString = "PREFIX  rdf:    <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " //
                  + "PREFIX   owl:  <http://www.w3.org/2002/07/owl#> " //
                  + "PREFIX  dbo:   <http://dbpedia.org/ontology/> " //
                  + "SELECT (COUNT(DISTINCT ?entity) as ?count) WHERE {?entity rdf:type dbo:" + entityType + "}";
            ResultSet resultSet = executer.executeQuery(queryString);
            int numberOfResults = resultSet.next().get("?count").asLiteral().getInt();
            System.out.println(entityType + ": " + numberOfResults);
         }
         executer.shutdown();
      }
   }

}
