package de.upb.vu.kgf.entitylinking;


import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.Test;

import de.upb.vu.kgf.TDBStorageLocations;
import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.fusion.creation.EntityCreator;
import de.upb.vu.kgf.fusion.entitylinking.DefaultEntityLinkingCandidateSetSelector;
import de.upb.vu.kgf.query.executor.SparqlTDBQueryExecutor;


public class DefaultEntityLinkingCandidateSetSelectorTest {

   @Test
   public void testSelectLinkingCandidates() {
      EntityCreator entityCreator = new EntityCreator(new SparqlTDBQueryExecutor(TDBStorageLocations.STORAGE_PATH_DE));
      Entity angelaMerkel = entityCreator.createEntity(new IRI("http://de.dbpedia.org/resource/Angela_Merkel"));
      DefaultEntityLinkingCandidateSetSelector linkingCandidateSetSelector = new DefaultEntityLinkingCandidateSetSelector(10, 0.35);
      List<Entity> linkingCandidates = linkingCandidateSetSelector.selectLinkingCandidates(angelaMerkel,
            TDBStorageLocations.STORAGE_PATH_NL);

      List<String> expected = Arrays.asList("http://nl.dbpedia.org/resource/Angela_Merkel", "http://nl.dbpedia.org/resource/Angela_Carter",
            "http://nl.dbpedia.org/resource/Una_Merkel", "http://nl.dbpedia.org/resource/Angela_Mortimer",
            "http://nl.dbpedia.org/resource/Angela_Thirkell", "http://nl.dbpedia.org/resource/Angela_Merici");

      assertEquals(expected.stream().map(s -> new IRI(s)).collect(Collectors.toSet()),
            linkingCandidates.stream().map(p -> p.getIRI()).collect(Collectors.toSet()));
   }
}
