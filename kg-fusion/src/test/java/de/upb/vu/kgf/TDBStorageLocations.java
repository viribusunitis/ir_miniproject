package de.upb.vu.kgf;


public class TDBStorageLocations {

   public static final String STORAGE_PATH = "/Users/alexanderhetzer/Development/courses_ws1718/workspace/tdb_storage";
   // public static final String STORAGE_PATH =
   // "/Users/Tanja/Development/Uni/workspace_ir_miniproject/tdb_storage";

   public static final String STORAGE_PATH_DE = STORAGE_PATH + "/de";
   public static final String STORAGE_PATH_NL = STORAGE_PATH + "/nl";
   public static final String STORAGE_PATH_PT = STORAGE_PATH + "/pt";
}
