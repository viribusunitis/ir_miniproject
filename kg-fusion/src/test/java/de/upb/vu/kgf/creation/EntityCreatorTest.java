package de.upb.vu.kgf.creation;


import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.upb.vu.kgf.TDBStorageLocations;
import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.datastructure.property.DateProperty;
import de.upb.vu.kgf.datastructure.property.IRIProperty;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.datastructure.property.StringProperty;
import de.upb.vu.kgf.fusion.creation.EntityCreator;
import de.upb.vu.kgf.query.executor.SparqlTDBQueryExecutor;


public class EntityCreatorTest {

   private static final String RAW_ANG_LEE_IRI = "http://de.dbpedia.org/resource/Ang_Lee";


   @Test
   public void testValidEntityCreation() {
      EntityCreator creator = new EntityCreator(new SparqlTDBQueryExecutor(TDBStorageLocations.STORAGE_PATH_DE));
      Entity entity = creator.createEntity(new IRI(RAW_ANG_LEE_IRI));
      Entity expectedEntity = getAngLeeEntity();
      assertEquals(expectedEntity, entity);
   }


   private Entity getAngLeeEntity() {
      Property typeProperty = new IRIProperty("rdf:type", "http://dbpedia.org/ontology/Person",
            new IRI("http://dbpedia.org/ontology/Person"));
      Property labelProperty = new StringProperty("rdfs:label", "Ang Lee@de", "Ang Lee");
      Property lccnProperty = new StringProperty("http://dbpedia.org/ontology/lccn", "no/94/38345", "no/94/38345");
      Property viafIdProperty = new StringProperty("http://dbpedia.org/ontology/viafId", "69129224", "69129224");
      String abstractString = "Ang Lee (chinesisch 李安, Pinyin Lǐ Ān; * 23. Oktober 1954 in Pingtung, Taiwan) ist ein US-amerikanisch-taiwanischer Filmregisseur, Drehbuchautor und Produzent. Er ist als vielfach ausgezeichneter Regisseur bekannt für so unterschiedliche Filme wie Eat Drink Man Woman, die Jane-Austen-Adaption Sinn und Sinnlichkeit, den Martial Arts-Film Tiger and Dragon sowie Brokeback Mountain, für den er 2006 den Regie-Oscar erhielt. Einen weiteren Oscar erhielt er 2013 für seine Regiearbeit an Life of Pi: Schiffbruch mit Tiger.";
      Property abstractProperty = new StringProperty("http://dbpedia.org/ontology/abstract", abstractString, abstractString);
      Property individualisedGndProperty = new StringProperty("http://dbpedia.org/ontology/individualisedGnd", "119317079", "119317079");
      Property birthDateProperty = new DateProperty("http://dbpedia.org/ontology/birthDate", "1954-10-23@xsd:date",
            LocalDate.parse("1954-10-23", DateTimeFormatter.ISO_DATE), true);
      Property birthPlaceProperty1 = new IRIProperty("http://dbpedia.org/ontology/birthPlace", "http://de.dbpedia.org/resource/Pingtung",
            new IRI("http://de.dbpedia.org/resource/Pingtung"));
      Property birthPlaceProperty2 = new IRIProperty("http://dbpedia.org/ontology/birthPlace",
            "http://de.dbpedia.org/resource/Republik_China_(Taiwan)", new IRI("http://de.dbpedia.org/resource/Republik_China_(Taiwan)"));
      Property sameAsProperty = new IRIProperty("owl:sameAs", "http://www.viaf.org/viaf/69129224",
            new IRI("http://www.viaf.org/viaf/69129224"));
      Property nameProperty = new StringProperty("http://xmlns.com/foaf/0.1/name", "Lee, Ang@de", "Lee, Ang");

      List<Property> properties = Arrays.asList(typeProperty, labelProperty, lccnProperty, viafIdProperty, abstractProperty,
            individualisedGndProperty, birthDateProperty, birthPlaceProperty1, birthPlaceProperty2, sameAsProperty, nameProperty);
      return new Entity(new IRI(RAW_ANG_LEE_IRI), properties);
   }
}
