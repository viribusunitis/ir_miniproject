package de.upb.vu.kgf.evaluation.metric.ir;


import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.datastructure.property.DefaultProperty;
import de.upb.vu.kgf.datastructure.property.IRIProperty;
import de.upb.vu.kgf.datastructure.property.NumericalProperty;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.datastructure.property.StringProperty;
import de.upb.vu.kgf.evaluation.metric.EvaluationMetric;


public class IREvaluationMetricTest {


   @Test
   public void testRecallGetMetricResultForIdenticalObjects() {
      EvaluationMetric metric = new IRRecallEvaluationMetric();
      double metricResult = metric.getMetricResult(getTestEntity(), getTestEntity());
      assertEquals(1.0, metricResult, 0.0001);
   }


   @Test
   public void testPrecisionGetMetricResultForIdenticalObjects() {
      EvaluationMetric metric = new IRPrecisionEvaluationMetric();
      double metricResult = metric.getMetricResult(getTestEntity(), getTestEntity());
      assertEquals(1.0, metricResult, 0.0001);
   }


   @Test
   public void testRecallGetMetricResultForIntersectingObjects() {
      EvaluationMetric metric = new IRRecallEvaluationMetric();
      double metricResult = metric.getMetricResult(getExtendedTestEntity(), getTestEntity());
      assertEquals(3 / 4.0, metricResult, 0.0001);
   }


   @Test
   public void testPrecisionGetMetricResultForIntersectingObjects() {
      EvaluationMetric metric = new IRPrecisionEvaluationMetric();
      double metricResult = metric.getMetricResult(getExtendedTestEntity(), getTestEntity());
      assertEquals(1.0, metricResult, 0.0001);
   }


   @Test
   public void testRecallGetMetricResultForObjectsWithDifferentPropertyValues() {
      EvaluationMetric metric = new IRRecallEvaluationMetric();
      double metricResult = metric.getMetricResult(getTestEntityWithOneDifferentValue(), getTestEntity());
      assertEquals(2 / 3.0, metricResult, 0.0001);
   }


   @Test
   public void testPrecisionGetMetricResultForObjectsWithDifferentPropertyValues() {
      EvaluationMetric metric = new IRPrecisionEvaluationMetric();
      double metricResult = metric.getMetricResult(getTestEntityWithOneDifferentValue(), getTestEntity());
      assertEquals(2 / 3.0, metricResult, 0.0001);
   }


   private Entity getTestEntity() {
      Entity entity = new Entity(new IRI("coolEntity"), getTestProperties());
      return entity;
   }


   private Entity getTestEntityWithOneDifferentValue() {
      Entity entity = new Entity(new IRI("coolEntity"), getTestPropertiesWithOneDifferentValue());
      return entity;
   }


   private Entity getExtendedTestEntity() {
      List<Property> properties = new ArrayList<>(getTestProperties());
      properties.add(new DefaultProperty("s", "ss"));
      Entity entity = new Entity(new IRI("extendedCoolEntity"), properties);
      return entity;
   }


   private List<Property> getTestProperties() {
      Property stringProperty = new StringProperty("string", "coolString", "coolString");
      Property iriProperty = new IRIProperty("iri", "coolIri", new IRI("coolIri"));
      Property numericalProperty = new NumericalProperty("numerical", "42", 42);
      return Arrays.asList(stringProperty, iriProperty, numericalProperty);
   }


   private List<Property> getTestPropertiesWithOneDifferentValue() {
      Property stringProperty = new StringProperty("string", "coolString", "coolString");
      Property iriProperty = new IRIProperty("iri", "coolIri", new IRI("coolIri"));
      Property numericalProperty = new NumericalProperty("numerical", "62", 62);
      return Arrays.asList(stringProperty, iriProperty, numericalProperty);
   }
}
