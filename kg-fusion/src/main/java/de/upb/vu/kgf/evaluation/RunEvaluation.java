package de.upb.vu.kgf.evaluation;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import de.upb.vu.kgf.Language;
import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.evaluation.entity.EvaluationEntityReader;
import de.upb.vu.kgf.evaluation.metric.EvaluationMetric;
import de.upb.vu.kgf.evaluation.metric.ir.IRPrecisionEvaluationMetric;
import de.upb.vu.kgf.evaluation.metric.ir.IRRecallEvaluationMetric;
import de.upb.vu.kgf.fusion.DefaultGraphFusionAlgorithm;
import de.upb.vu.kgf.fusion.GraphFusionAlgorithm;
import de.upb.vu.kgf.fusion.entitylinking.DefaultEntityLinkingCandidateSetSelector;
import de.upb.vu.kgf.fusion.entitylinking.EntityLinker;
import de.upb.vu.kgf.fusion.entitylinking.EntityLinkingCandidateSetSelector;
import de.upb.vu.kgf.fusion.entitylinking.EntitySimilarityBasedLinker;
import de.upb.vu.kgf.fusion.entitylinking.similarity.JaccardPropertyEntitySimilarityMeasure;
import de.upb.vu.kgf.fusion.entitymerging.DefaultEntityMerger;
import de.upb.vu.kgf.fusion.entitymerging.EntityMerger;


public class RunEvaluation {

   public static void main(String[] args) {
      if (args.length != 3) {
         System.err.println("Expecting 3 command line parameters.\n" + "1) path to TDB storage directory\n" + "2) path to input dataset\n"
               + "3) path to benchmark data directory ");
         return;
      }

      String tdbStorageDirectory = args[0];
      EntityLinkingCandidateSetSelector entityLinkingCandidateSetSelector = new DefaultEntityLinkingCandidateSetSelector();
      EntityLinker entityLinker = new EntitySimilarityBasedLinker(Arrays.asList(new JaccardPropertyEntitySimilarityMeasure()));
      EntityMerger entityMerger = new DefaultEntityMerger();
      GraphFusionAlgorithm graphFusionAlgorithm = new DefaultGraphFusionAlgorithm(tdbStorageDirectory, entityLinkingCandidateSetSelector,
            entityLinker, entityMerger);

      List<IRI> irisToMerge;
      try {
         irisToMerge = EvaluationEntityReader.readIRIs(args[1]);
      } catch (IOException e) {
         System.err.println("Could not read input dataset at " + args[1] + ".\n" + e);
         return;
      }

      Language sourceLanguage = Language.GERMAN;
      List<Language> languagesToMergeWith = Arrays.asList(Language.DUTCH, Language.PORTUGUESE);

      List<Entity> expectedMergedEntities;
      try {
         expectedMergedEntities = EvaluationEntityReader.read(args[2]);
      } catch (IOException e) {
         System.err.println("Could not read benchmark data at " + args[2] + ".\n" + e);
         return;
      }

      List<EvaluationMetric> evaluationMetrics = Arrays.asList(new IRRecallEvaluationMetric(), new IRPrecisionEvaluationMetric());
      EvaluationSetting evaluationSetting = new EvaluationSetting(graphFusionAlgorithm, evaluationMetrics, irisToMerge, sourceLanguage,
            languagesToMergeWith, expectedMergedEntities);

      Evaluation evaluation = new DefaultEvaluation();
      evaluation.evaluate(evaluationSetting);
   }

}
