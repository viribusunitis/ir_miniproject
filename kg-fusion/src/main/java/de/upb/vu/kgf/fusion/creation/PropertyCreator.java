package de.upb.vu.kgf.fusion.creation;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoField;

import org.apache.jena.datatypes.DatatypeFormatException;
import org.apache.jena.datatypes.xsd.XSDDateTime;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.RDFNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.datastructure.property.DateProperty;
import de.upb.vu.kgf.datastructure.property.DefaultProperty;
import de.upb.vu.kgf.datastructure.property.IRIProperty;
import de.upb.vu.kgf.datastructure.property.NumericalProperty;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.datastructure.property.StringProperty;


public class PropertyCreator {

   private static final Logger LOGGER = LoggerFactory.getLogger(PropertyCreator.class);


   public static Property createProperty(RDFNode predicate, RDFNode object) {
      String propertyType = predicate.toString();
      try {
         if (object.isLiteral()) {
            Literal literal = object.asLiteral();
            Object literalValue = literal.getValue();
            if (literalValue instanceof String) {
               return new StringProperty(propertyType, literal.toString(), literal.getString());
            } else if (literalValue instanceof Number) {
               return new NumericalProperty(propertyType, literal.toString(), (Number) literalValue);
            } else if (literalValue instanceof XSDDateTime) {
               return convertToDateProperty(propertyType, literalValue.toString());
            }
         } else if (object.isURIResource()) {
            String iri = object.asResource().getURI();
            return new IRIProperty(propertyType, object.toString(), new IRI(iri));
         }
      } catch (DatatypeFormatException exception) {
         LOGGER.debug("Problem while creating property \"{}\".", object.toString(), exception);
         return convertToDateProperty(propertyType, object.toString());
      }
      return new DefaultProperty(propertyType, object.toString());
   }


   private static Property convertToDateProperty(String propertyType, String rawValue) {
      try {
         String string = rawValue //
               .replace("^^http://www.w3.org/2001/XMLSchema#date", "") //
               .replace("^^http://www.w3.org/2001/XMLSchema#gYear", "") //
               .replaceAll("\"", "");
         LocalDate localDate = null;
         boolean fullDate = true;

         String[] date = string.split("-");
         if (date.length == 1) {
            DateTimeFormatter format;
            if (string.length() == 2) {
               format = new DateTimeFormatterBuilder().appendPattern("yy").parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                     .parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
            } else {
               format = new DateTimeFormatterBuilder().appendPattern("yyyy").parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                     .parseDefaulting(ChronoField.DAY_OF_MONTH, 1).toFormatter();
            }
            localDate = LocalDate.parse(string, format);
            fullDate = false;
         } else if (date.length == 3) {
            if (date[1].length() == 1) {
               if (date[2].length() == 1) {
                  localDate = LocalDate.parse(string, DateTimeFormatter.ofPattern("yyyy-M-d"));
               } else {
                  localDate = LocalDate.parse(string, DateTimeFormatter.ofPattern("yyyy-M-dd"));
               }
            } else {
               if (date[2].length() == 1) {
                  localDate = LocalDate.parse(string, DateTimeFormatter.ofPattern("yyyy-MM-d"));
               } else {
                  localDate = LocalDate.parse(string, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
               }
            }
         } else {
            LOGGER.debug("Cannot parse date string: {}", rawValue);
            return new DefaultProperty(propertyType, rawValue);
         }
         return new DateProperty(propertyType, rawValue, localDate, fullDate);
      } catch (DateTimeParseException exception) {
         LOGGER.debug("Cannot parse date string: {}", rawValue, exception);
         return new DefaultProperty(propertyType, rawValue);
      }
   }
}
