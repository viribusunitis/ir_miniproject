package de.upb.vu.kgf.query;


import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringJoiner;

import de.upb.vu.kgf.util.IOUtils;


public class QueryUtil {

   public static String getQueryString(String pathToQueryFile) {
      InputStream queryFileInputStream = QueryStore.class.getClassLoader().getResourceAsStream(pathToQueryFile);
      try {
         String nonPrefixedQueryString = IOUtils.readStringFromInputStream(queryFileInputStream);
         StringJoiner queryStringJoiner = new StringJoiner("\n ");
         List<String> prefixes = QueryUtil.getPrefixes();
         for (String prefix : prefixes) {
            queryStringJoiner.add(prefix);
         }
         queryStringJoiner.add(nonPrefixedQueryString);
         return queryStringJoiner.toString();
      } catch (IOException e) {
         throw new RuntimeException(e);
      }
   }


   public static List<String> getPrefixes() {
      InputStream prefixFileInputStream = QueryUtil.class.getClassLoader().getResourceAsStream("queries/prefixes.txt");
      List<String> prefixes = new ArrayList<>();
      try (Scanner scanner = new Scanner(prefixFileInputStream)) {
         while (scanner.hasNextLine()) {
            String prefix = scanner.nextLine();
            prefixes.add(prefix.trim());
         }
      }
      return prefixes;
   }
}
