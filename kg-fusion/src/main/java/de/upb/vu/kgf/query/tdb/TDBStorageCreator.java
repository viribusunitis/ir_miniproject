package de.upb.vu.kgf.query.tdb;


import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.List;

import org.apache.commons.compress.compressors.CompressorException;
import org.apache.commons.compress.compressors.CompressorInputStream;
import org.apache.commons.compress.compressors.CompressorStreamFactory;
import org.apache.jena.query.Dataset;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.tdb.TDBFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TDBStorageCreator {

   private static final Logger LOGGER = LoggerFactory.getLogger(TDBStorageCreator.class);

   private String directoryPath;
   private List<String> dumpFilePaths;


   public TDBStorageCreator(String directoryPath, List<String> dumpFilePaths) {
      super();
      this.directoryPath = directoryPath;
      this.dumpFilePaths = dumpFilePaths;
   }


   public void createTDBStore() throws FileNotFoundException, CompressorException {
      Dataset dataset = TDBFactory.createDataset(directoryPath);
      Model model = dataset.getDefaultModel();
      for (String dumpFilePath : dumpFilePaths) {
         LOGGER.info("Reading file {}.", dumpFilePath);
         if (dumpFilePath.endsWith("bz2")) {
            InputStream dumpInputStream = getBufferedReaderForCompressedFile(dumpFilePath);
            model.read(dumpInputStream, null, "TTL");
         } else {
            model.read(dumpFilePath);
         }
      }
      model.close();
      dataset.close();
   }


   public static InputStream getBufferedReaderForCompressedFile(String filePath) throws FileNotFoundException, CompressorException {
      FileInputStream fileInputStream = new FileInputStream(filePath);
      BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
      CompressorInputStream compressorInputStream = new CompressorStreamFactory().createCompressorInputStream(bufferedInputStream);
      return compressorInputStream;
   }
}
