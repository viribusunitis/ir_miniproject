package de.upb.vu.kgf.util;


/**
 * This util class offers convenience methods for {@code String}.
 * 
 * @author Tanja Tornede
 *
 */
public class StringUtil {

   /** Constant empty string. */
   public static final String EMPTY_STRING = "";
   /** Constant single whitespace. */
   public static final String SINGLE_WHITESPACE = " ";
   /** Constant dot. */
   public static final String DOT = ".";
   /** A single colon. */
   public static final String COLON = ":";
   /** Constant start. */
   public static final String STAR = "*";
   /** Constant dash. */
   public static final String DASH = "-";
   /** Constant underline. */
   public static final String UNDERLINE = "_";
   /** Constant comma. */
   public static final String COMMA = ",";
   /** Constant hash tag. */
   public static final String HASHTAG = "#";
   /** Constant bitwise OR. */
   public static final String BITWISEOR = "|";
   /** Constant slash. */
   public static final String SLASH = "/";
   /** Constant backslash. */
   public static final String BACKSLASH = "\\";
   /** Constant equals sign. */
   public static final String EQUALS = "=";
   /** The opening round bracket: ( */
   public static final String BRACKET_ROUND_OPEN = "(";
   /** The closing round bracket: ) */
   public static final String BRACKET_ROUND_CLOSE = ")";

   /** Constant tab space. */
   public static final String TAB_SPACE = "\t";
   /** Constant system dependent line break. */
   public static final String LINE_BREAK = System.getProperty("line.separator");

   public static final String REGEX_PUNCTUATION = "\\p{P}";
   public static final String REGEX_WHITESPACE = "\\s";
   public static final String REGEX_TAB = "\\t";

   /** The exception message to hide the utility class constructor */
   public static final String EXCEPTION_MESSAGE_ACCESS_ERROR = "Utility class";


   /**
    * Hides the public constructor.
    */
   private StringUtil() {
      throw new IllegalAccessError(EXCEPTION_MESSAGE_ACCESS_ERROR);
   }


   /**
    * Replaces multiple white spaces with a single one in the given {@link String} and returns the
    * result as {@link String}.
    * 
    * @param string The {@link String} to replace whitespaces at.
    * @return The edited {@link String}.
    */
   public static String removeMultipleWhitespaces(String string) {
      return string.trim().replaceAll("\\s+", SINGLE_WHITESPACE);
   }


   /**
    * Merges a {@link String} array into a single {@link String} where elements are separated by the
    * given separator {@link String}.
    * 
    * @param array The array to merge.
    * @param seperator The separator to use.
    * @return The merged {@link String}.
    */
   public static String mergeStringArrayToStringWithSeperator(String[] array, String seperator) {
      StringBuilder builder = new StringBuilder();
      for (int i = 0; i < array.length; i++) {
         builder.append(array[i]);
         if (i < array.length - 1) {
            builder.append(seperator);
         }
      }
      return builder.toString();
   }


   /**
    * Returns a {@link String} consisting only of whitespace of the given length.
    * 
    * @param length The length of the whitespace {@link String} to be produced.
    * @return The {@link String} consisting only of whitespace of the given length.
    */
   public static String getWhitespaceStringOfLength(int length) {
      return repeat(StringUtil.SINGLE_WHITESPACE, length);
   }


   /**
    * Returns a {@link String} consisting only of the repetitions of the given {@link String}, based
    * on the given number of repetitions.
    * 
    * @param string The {@link String} to be repeated.
    * @param numberOfRepetitions The number of repetitions.
    * @return A {@link String} consisting of the given number of repetitions of the given
    *         {@link String}.
    */
   public static String repeat(String string, int numberOfRepetitions) {
      StringBuilder outputBuilder = new StringBuilder(numberOfRepetitions);
      for (int i = 0; i < numberOfRepetitions; i++) {
         outputBuilder.append(string);
      }
      return outputBuilder.toString();
   }


   /**
    * Checks whether the given {@link String} is either {@code null} or empty.
    * 
    * @param string The {@link String} to check.
    * @return {@code true} if the given {@link String} is {@code null} or empty.
    */
   public static boolean isNullOrEmpty(String string) {
      return string == null || string.isEmpty();
   }
}

