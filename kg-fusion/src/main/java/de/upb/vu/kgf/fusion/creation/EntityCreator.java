package de.upb.vu.kgf.fusion.creation;


import java.util.ArrayList;
import java.util.List;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.RDFNode;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.query.QueryStore;
import de.upb.vu.kgf.query.executor.QueryExecutor;


public class EntityCreator {

   private QueryExecutor queryExecutor;


   public EntityCreator(QueryExecutor queryExecutor) {
      this.queryExecutor = queryExecutor;
   }


   public Entity createEntity(IRI entityIRI) {
      String rawQueryString = QueryStore.FETCH_ALL_PROPERTIES_OF_IRI.getQueryString();
      rawQueryString = rawQueryString.replaceAll("\\$IRI\\$", entityIRI.toString());
      ResultSet resultSet = queryExecutor.executeQuery(rawQueryString);
      List<Property> properties = new ArrayList<>();
      while (resultSet.hasNext()) {
         QuerySolution solution = resultSet.next();
         RDFNode predicate = solution.get("?p");
         if (predicate == null) {
            throw new RuntimeException("Cannot create property as ?p variable is missing in query result.");
         }
         RDFNode object = solution.get("?o");
         if (object == null) {
            throw new RuntimeException("Cannot create property as ?o variable is missing in query result.");
         }
         Property property = PropertyCreator.createProperty(predicate, object);
         properties.add(property);
      }
      queryExecutor.closeLastQuery();
      return new Entity(entityIRI, properties);
   }


   public void shutdown() {
      queryExecutor.shutdown();
   }

}
