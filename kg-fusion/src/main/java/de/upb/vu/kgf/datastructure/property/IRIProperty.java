package de.upb.vu.kgf.datastructure.property;


import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.util.StringUtil;


public class IRIProperty extends AbstractProperty {

   private IRI value;


   public IRIProperty(String type, String rawValue, IRI value) {
      super(type, rawValue);
      this.value = value;
   }


   public IRI getValue() {
      return value;
   }


   public void setValue(IRI value) {
      this.value = value;
      this.rawValue = this.value.getIdentifier();
   }


   @Override
   public String asRDF() {
      return "<" + type + ">" + StringUtil.SINGLE_WHITESPACE + "<" + value.toString() + ">";
   }


   @Override
   public String toString() {
      return value.toString();
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + ((value == null) ? 0 : value.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (!super.equals(obj)) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      IRIProperty other = (IRIProperty) obj;
      if (value == null) {
         if (other.value != null) {
            return false;
         }
      } else if (!value.equals(other.value)) {
         return false;
      }
      return true;
   }

}
