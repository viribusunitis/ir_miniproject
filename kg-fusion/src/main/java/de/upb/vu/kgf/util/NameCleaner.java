package de.upb.vu.kgf.util;


public class NameCleaner {

   public static String cleanName(String name) {
      return name.replaceAll("\\p{Punct}", " ");
   }
}
