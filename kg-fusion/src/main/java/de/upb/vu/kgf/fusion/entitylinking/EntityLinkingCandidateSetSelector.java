package de.upb.vu.kgf.fusion.entitylinking;


import java.util.List;

import de.upb.vu.kgf.datastructure.Entity;


public interface EntityLinkingCandidateSetSelector {

   public List<Entity> selectLinkingCandidates(Entity entity, String tdbStorageOfTargetGraph);
}
