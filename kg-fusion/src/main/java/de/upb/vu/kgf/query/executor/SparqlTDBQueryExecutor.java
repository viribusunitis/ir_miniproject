package de.upb.vu.kgf.query.executor;


import org.apache.jena.query.Dataset;
import org.apache.jena.tdb.TDBFactory;


public class SparqlTDBQueryExecutor extends AbstractQueryExecutor {

   private Dataset dataset;


   public SparqlTDBQueryExecutor(String tdbDirectoryPath) {
      super(false);
      dataset = TDBFactory.createDataset(tdbDirectoryPath);
      datasetModel = dataset.getDefaultModel();
   }


   @Override
   public void shutdown() {
      dataset.close();
   }

}
