package de.upb.vu.kgf.query.executor;


import org.apache.jena.query.ResultSet;


public interface QueryExecutor {

   public ResultSet executeQuery(String queryString);


   public boolean executeAskQuery(String askQueryString);


   public void closeLastQuery();


   public void shutdown();

}
