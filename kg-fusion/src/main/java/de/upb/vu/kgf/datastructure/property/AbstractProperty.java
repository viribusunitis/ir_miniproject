package de.upb.vu.kgf.datastructure.property;


import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.util.StringUtil;


public abstract class AbstractProperty implements Property {

   protected IRI type;
   protected String rawValue;


   public AbstractProperty(String type, String rawValue) {
      super();
      this.type = new IRI(type);
      this.rawValue = rawValue;
   }


   @Override
   public IRI getType() {
      return type;
   }


   @Override
   public String getRawValue() {
      return rawValue;
   }


   @Override
   public String asRDF() {
      return "<" + type + ">" + StringUtil.SINGLE_WHITESPACE + rawValue;
   }


   @Override
   public String toString() {
      return "[type=" + type + ", rawValue=" + rawValue + "]";
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((type == null) ? 0 : type.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      AbstractProperty other = (AbstractProperty) obj;
      if (type == null) {
         if (other.type != null) {
            return false;
         }
      } else if (!type.equals(other.type)) {
         return false;
      }
      return true;
   }


}
