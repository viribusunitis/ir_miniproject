package de.upb.vu.kgf.fusion.entitylinking.similarity;


import java.util.HashSet;
import java.util.Set;

import org.apache.jena.ext.com.google.common.collect.Sets;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.datastructure.property.IRIProperty;
import de.upb.vu.kgf.datastructure.property.Property;


public class JaccardPropertyEntitySimilarityMeasure implements EntitySimilarityMeasure {

   @Override
   public double computeEntitySimilarity(Entity entity1, Entity entity2) {
      Set<Property> properties1 = getNormalizedProperties(entity1);
      Set<Property> properties2 = getNormalizedProperties(entity2);
      return Sets.intersection(properties1, properties2).size() / (double) Sets.union(properties1, properties2).size();
   }


   private Set<Property> getNormalizedProperties(Entity entity) {
      Set<Property> normalizedProperties = new HashSet<>();
      for (Property property : entity.getProperties()) {
         if (property instanceof IRIProperty) {
            IRIProperty iriProperty = (IRIProperty) property;
            IRI normalizedIRI = getNormalizedIRI(iriProperty.getValue());
            IRIProperty normalizedIriProperty = new IRIProperty(iriProperty.getType().getIdentifier(), normalizedIRI.getIdentifier(),
                  normalizedIRI);
            normalizedProperties.add(normalizedIriProperty);
         } else {
            normalizedProperties.add(property);
         }
      }
      return normalizedProperties;
   }


   private IRI getNormalizedIRI(IRI iri) {
      String[] splitIri = iri.getIdentifier().split("/resource/");
      return new IRI("http://example.com/" + splitIri[splitIri.length - 1]);
   }
}
