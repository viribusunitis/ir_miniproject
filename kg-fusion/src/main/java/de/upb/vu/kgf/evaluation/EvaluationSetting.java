package de.upb.vu.kgf.evaluation;


import java.util.List;
import java.util.StringJoiner;

import de.upb.vu.kgf.Language;
import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.evaluation.metric.EvaluationMetric;
import de.upb.vu.kgf.fusion.GraphFusionAlgorithm;


public class EvaluationSetting {

   private GraphFusionAlgorithm graphFusionAlgorithm;

   private List<EvaluationMetric> evaluationMetrics;

   private List<IRI> irisToMerge;
   private Language sourceLanguage;
   private List<Language> languagesToMergeWith;

   private List<Entity> expectedMergedEntities;


   public EvaluationSetting(GraphFusionAlgorithm graphFusionAlgorithm, List<EvaluationMetric> evaluationMetrics, List<IRI> irisToMerge,
         Language sourceLanguage, List<Language> languagesToMergeWith, List<Entity> expectedMergedEntities) {
      super();
      this.graphFusionAlgorithm = graphFusionAlgorithm;
      this.evaluationMetrics = evaluationMetrics;
      this.irisToMerge = irisToMerge;
      this.sourceLanguage = sourceLanguage;
      this.languagesToMergeWith = languagesToMergeWith;
      this.expectedMergedEntities = expectedMergedEntities;
   }


   public GraphFusionAlgorithm getGraphFusionAlgorithm() {
      return graphFusionAlgorithm;
   }


   public List<IRI> getIrisToMerge() {
      return irisToMerge;
   }


   public Language getSourceLanguage() {
      return sourceLanguage;
   }


   public List<Language> getLanguagesToMergeWith() {
      return languagesToMergeWith;
   }


   public List<EvaluationMetric> getEvaluationMetrics() {
      return evaluationMetrics;
   }


   public List<Entity> getExpectedMergedEntities() {
      return expectedMergedEntities;
   }


   @Override
   public String toString() {
      StringJoiner lineJoiner = new StringJoiner("\n");
      lineJoiner.add("algorithm: " + graphFusionAlgorithm.toString());
      lineJoiner.add("metrics: " + evaluationMetrics.toString());
      lineJoiner.add("source language: " + sourceLanguage.toString());
      lineJoiner.add("languages to merge with: " + languagesToMergeWith.toString());
      return lineJoiner.toString();
   }


}
