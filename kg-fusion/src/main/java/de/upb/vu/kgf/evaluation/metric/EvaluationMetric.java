package de.upb.vu.kgf.evaluation.metric;


import java.util.List;

import de.upb.vu.kgf.datastructure.Entity;


public interface EvaluationMetric {

   public double getMetricResult(Entity expectedEntity, Entity computedEntity);


   public double getMetricResult(List<Entity> expectedEntities, List<Entity> computedEntities);


   public String getName();

}
