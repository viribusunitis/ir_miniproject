package de.upb.vu.kgf.query.arq;


import org.apache.jena.sparql.expr.ExprEvalException;
import org.apache.jena.sparql.expr.NodeValue;
import org.apache.jena.sparql.function.FunctionBase2;

import de.upb.vu.kgf.nlp.LevenstheinDistanceComputer;
import de.upb.vu.kgf.util.NameCleaner;


public class AQRLevenstheinDistance extends FunctionBase2 {

   @Override
   public NodeValue exec(NodeValue v1, NodeValue v2) {
      if (v1.getNode() == null || v2.getString() == null) {
         throw new ExprEvalException("Either node value 1 or 2 is not an entity: " + v1 + " OR " + v2);
      }
      String node1AsString = NameCleaner.cleanName(v1.getNode().getLocalName());
      String node2AsString = v2.getString();

      double normalizedDistance = LevenstheinDistanceComputer.computeDistance(node1AsString, node2AsString);
      return NodeValue.makeDouble(normalizedDistance);
   }

}
