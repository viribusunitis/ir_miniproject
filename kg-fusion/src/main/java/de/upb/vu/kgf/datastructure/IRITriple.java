package de.upb.vu.kgf.datastructure;


public class IRITriple {

   private IRI de;
   private IRI nl;
   private IRI pt;


   public IRITriple(String[] iris) {
      super();
      if (iris.length != 3) {
         throw new RuntimeException();
      }
      this.de = new IRI(iris[0]);
      this.pt = new IRI(iris[1]);
      this.nl = new IRI(iris[2]);
   }


   public IRI getDe() {
      return de;
   }


   public IRI getNl() {
      return nl;
   }


   public IRI getPt() {
      return pt;
   }

}
