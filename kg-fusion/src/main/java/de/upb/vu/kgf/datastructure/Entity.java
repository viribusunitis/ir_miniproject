package de.upb.vu.kgf.datastructure;


import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.StringJoiner;

import de.upb.vu.kgf.datastructure.property.IRIProperty;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.datastructure.property.StringProperty;
import de.upb.vu.kgf.util.StringUtil;


public class Entity {

   private IRI iri;
   private Set<Property> properties;


   public Entity(IRI iri, List<Property> properties) {
      super();
      this.iri = iri;
      this.properties = new HashSet<>(properties);
   }


   public IRI getIRI() {
      return iri;
   }


   public void setIri(IRI iri) {
      this.iri = iri;
   }


   public String getLabel() {
      Optional<Property> x = properties.stream().filter(p -> p.getType().equals(PropertyTypeIRIs.LABEL)).findFirst();
      if (x.isPresent()) {
         return ((StringProperty) x.get()).getValue();
      }
      String[] splittedIRI = iri.getIdentifier().split(StringUtil.SLASH);
      return splittedIRI[splittedIRI.length - 1];
   }


   public IRI getType() {
      return properties.stream().filter(p -> (p instanceof IRIProperty)).map(p -> (IRIProperty) p)
            .filter(p -> p.getType().equals(PropertyTypeIRIs.ENTITY_TYPE)).findFirst().get().getValue();
   }


   public Set<Property> getProperties() {
      return properties;
   }


   public String asRDF() {
      List<String> propertiesAsRDF = new ArrayList<>();
      for (Property property : properties) {
         propertiesAsRDF
               .add("<" + iri + ">" + StringUtil.SINGLE_WHITESPACE + property.asRDF() + StringUtil.SINGLE_WHITESPACE + StringUtil.DOT);
      }
      Collections.sort(propertiesAsRDF);

      StringJoiner sb = new StringJoiner(StringUtil.LINE_BREAK);
      for (int i = 0; i < propertiesAsRDF.size(); i++) {
         sb.add(propertiesAsRDF.get(i));
      }
      return sb.toString();
   }


   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      sb.append(iri);
      sb.append(", properties:[");
      StringJoiner sj = new StringJoiner(", ");
      for (Property property : properties) {
         sj.add(property.toString());
      }
      sb.append(sj.toString());
      sb.append("]");
      return sb.toString();
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((iri == null) ? 0 : iri.hashCode());
      result = prime * result + ((properties == null) ? 0 : properties.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      Entity other = (Entity) obj;
      if (iri == null) {
         if (other.iri != null) {
            return false;
         }
      } else if (!iri.equals(other.iri)) {
         return false;
      }
      if (properties == null) {
         if (other.properties != null) {
            return false;
         }
      } else if (!(new HashSet<>(properties)).equals((new HashSet<>(other.properties)))) {
         return false;
      }
      return true;
   }

}
