package de.upb.vu.kgf.fusion.entitymerging;


import de.upb.vu.kgf.datastructure.Entity;


public interface EntityMergingPostProcessor {

   public Entity postProcess(Entity entity);
}
