package de.upb.vu.kgf.util;


import de.upb.vu.kgf.Language;


public class TDBUtil {

   public static String getCompleteTDBDirectory(String generalTDBDirectory, Language language) {
      return generalTDBDirectory + "/" + language.getAbbreviation();
   }
}
