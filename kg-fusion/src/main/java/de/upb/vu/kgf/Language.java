package de.upb.vu.kgf;


public enum Language {
   GERMAN("de"),
   PORTUGUESE("pt"),
   DUTCH("nl");

   private String abbreviation;


   private Language(String abbreviation) {
      this.abbreviation = abbreviation;
   }


   public String getAbbreviation() {
      return abbreviation;
   }
}
