package de.upb.vu.kgf.util;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.IntStream;


/**
 * This util class offers convenience methods for Collections.
 * 
 * @author Tanja Tornede
 *
 */
public class CollectionsUtil {

   private static final String ERROR_GIVEN_TWO_ARRAYS_HAVE_TO_BE_OF_SAME_LENGTH = "The given two arrays have to be of the same length.";


   /**
    * Hides the public constructor.
    */
   private CollectionsUtil() {
      throw new IllegalAccessError(StringUtil.EXCEPTION_MESSAGE_ACCESS_ERROR);
   }


   /**
    * Returns a deep copy of the given {@link List}.
    * 
    * @param <T> The type of the {@link List} to copy.
    * 
    * @param listToCopy The {@link List} to copy.
    * @return A copy of the given {@link List}.
    */
   public static <T> List<T> getDeepCopyOf(List<T> listToCopy) {
      List<T> copy = new ArrayList<>(listToCopy);
      Collections.copy(copy, listToCopy);
      return copy;
   }


   /**
    * Returns a deep copy of the given {@link Map}.
    * 
    * @param <KEY_TYPE> The type of the key of the {@link Map}.
    * @param <VALUE_TYPE> The type of the value of the {@link Map}.
    * 
    * @param original The {@link Map} to copy.
    * @return The copy of the given {@link Map}.
    */
   public static <KEY_TYPE, VALUE_TYPE> Map<KEY_TYPE, VALUE_TYPE> getdeepCopyOfMap(Map<KEY_TYPE, VALUE_TYPE> original) {
      Map<KEY_TYPE, VALUE_TYPE> copy = new HashMap<>();
      for (Entry<KEY_TYPE, VALUE_TYPE> entry : original.entrySet()) {
         copy.put(entry.getKey(), entry.getValue());
      }
      return copy;
   }


   /**
    * Copies the values of the given {@link List} into an {@code int[]} array.
    * 
    * @param integerList The {@link List} containing the values to be copied.
    * @return The new {@code int[]} array.
    */
   public static int[] convertIntegerListToArray(List<Integer> integerList) {
      int[] copy = new int[integerList.size()];
      for (int i = 0; i < integerList.size(); i++) {
         copy[i] = integerList.get(i);
      }
      return copy;
   }


   /**
    * Copies the values of the given {@link List} into a {@code double[]} array.
    * 
    * @param doubleList The {@link List} containing the values to be copied.
    * @return The new {@code double[]} array.
    */
   public static double[] convertDoubleListToArray(List<Double> doubleList) {
      double[] copy = new double[doubleList.size()];
      for (int i = 0; i < doubleList.size(); i++) {
         copy[i] = doubleList.get(i);
      }
      return copy;
   }


   /**
    * Returns the sorted key values of a {@link Map} in decreasing value order.
    *
    * @param map The {@link Map} with integer keys and double values.
    * @return The sorted key values in decreasing value order.
    */
   public static int[] getSortedKeyValuesInDecreasingValueOrder(Map<Integer, Double> map) {
      ArrayList<Integer> rankAggregationArrayList = new ArrayList<>();
      Comparator<Entry<Integer, Double>> comperatorByDecreasing = (e1, e2) -> e2.getValue().compareTo(e1.getValue());
      map.entrySet().stream().sorted(comperatorByDecreasing).forEachOrdered(e -> rankAggregationArrayList.add(e.getKey()));
      return convertIntegerListToArray(rankAggregationArrayList);
   }


   /**
    * Returns the sorted key values in increasing value order of a map.
    *
    * @param map The {@link Map} with integer keys and double values.
    * @return The sorted key values in increasing value order.
    */
   public static int[] getSortedKeyValuesInIncreasingOrder(Map<Integer, Double> map) {
      ArrayList<Integer> rankAggregationArrayList = new ArrayList<>();
      Comparator<Entry<Integer, Double>> comperatorByIncreasingValue = (e1, e2) -> e1.getValue().compareTo(e2.getValue());
      map.entrySet().stream().sorted(comperatorByIncreasingValue).forEachOrdered(e -> rankAggregationArrayList.add(e.getKey()));
      return convertIntegerListToArray(rankAggregationArrayList);
   }


   /**
    * Converts the given {@link String} array to a double array.
    * 
    * @param stringArray The {@link String} array to convert.
    * @return The converted {@link Double} array.
    */
   public static double[] convertStringArrayToDoubleArray(String[] stringArray) {
      double[] doubleArray = new double[stringArray.length];
      for (int i = 0; i < stringArray.length; i++) {
         doubleArray[i] = Double.parseDouble(stringArray[i]);
      }
      return doubleArray;
   }


   /**
    * Returns the index of the given integer element in the provided array of integers.
    * 
    * @param array The int array to search.
    * @param valueToBeSearched The object to be searched.
    * @return The index of object double array.
    */
   public static int indexOfValueInIntegerArray(int[] array, int valueToBeSearched) {
      int index = -1;
      for (int i = 0; i < array.length; i++) {
         if (array[i] == valueToBeSearched) {
            index = i;
         }
      }
      return index;
   }


   /**
    * Extends the given array by the given value.
    * 
    * @param originalArray The array to extend
    * @param additionalValue The value to add at the end of the array.
    * 
    * @return An array containing the given original array, extended by the given additional value.
    */
   public static double[] extendArray(double[] originalArray, double additionalValue) {
      double[] newArray = new double[originalArray.length + 1];
      for (int i = 0; i < originalArray.length; i++) {
         newArray[i] = originalArray[i];
      }
      newArray[originalArray.length] = additionalValue;
      return newArray;
   }


   /**
    * Creates a list of integers from [0, n-1] for the given n.
    * 
    * @param n The amount of indices.
    * @return The list of the indices created.
    */
   public static List<Integer> createListOfIndices(int n) {
      List<Integer> listOfIndices = new ArrayList<>();
      for (int i = 0; i < n; i++) {
         listOfIndices.add(i);
      }
      return listOfIndices;
   }


   /**
    * Returns the indices of the given array sorted in an decreasing order according to the values
    * located inside the cells.
    * 
    * @param values The array of which the sorted indices should be determined
    * @return The indices of the given array sorted in an decreasing order according to the values
    *         located inside the cells.
    */
   public static int[] getIndicesSortedDecreasinglyAccordingToValue(double[] values) {
      return IntStream.range(0, values.length).boxed().sorted((i, j) -> Double.compare(values[j], values[i])).mapToInt(value -> value)
            .toArray();
   }


   /**
    * Normalizes the given {@code vector}.
    * 
    * @param vector The vector to normalize.
    */
   public static void normalize(double[] vector) {
      double sum = 0;
      for (double entry : vector) {
         sum += entry;
      }
      for (int i = 0; i < vector.length; i++) {
         vector[i] = vector[i] / sum;
      }
   }


   /**
    * Returns the absolute difference between the given vectors.
    * 
    * @param vector1 The first vector.
    * @param vector2 The second vector.
    * @return The absolute difference between the given vectors.
    */
   public static double getAbsoluteDifferenceBetween(double[] vector1, double[] vector2) {
      if (vector1.length != vector2.length) {
         throw new IllegalArgumentException(ERROR_GIVEN_TWO_ARRAYS_HAVE_TO_BE_OF_SAME_LENGTH);
      }
      double difference = 0;
      for (int i = 0; i < vector1.length; i++) {
         difference += Math.abs(vector1[i] - vector2[i]);
      }
      return difference;
   }

}

