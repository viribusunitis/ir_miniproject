package de.upb.vu.kgf.evaluation;


import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.StringJoiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.evaluation.metric.EvaluationMetric;
import de.upb.vu.kgf.fusion.EntityWriter;
import de.upb.vu.kgf.fusion.GraphFusionAlgorithm;
import de.upb.vu.kgf.util.IOUtils;


public class DefaultEvaluation implements Evaluation {

   private static final Logger LOGGER = LoggerFactory.getLogger(DefaultEvaluation.class);


   @Override
   public EvaluationResult evaluate(EvaluationSetting setting) {
      GraphFusionAlgorithm algorithm = setting.getGraphFusionAlgorithm();
      List<IRI> irisToMerge = setting.getIrisToMerge();

      List<Entity> mergedEntities = algorithm.runFusion(irisToMerge, setting.getSourceLanguage(), setting.getLanguagesToMergeWith());
      try {
         EntityWriter.writeEntitiesToFile("results/", mergedEntities);
      } catch (IOException e) {
         LOGGER.error("Could not print merged entities.", e);
      }

      List<Entity> expectedMergedEntities = setting.getExpectedMergedEntities();

      EvaluationResult evaluationResult = new EvaluationResult();
      for (EvaluationMetric metric : setting.getEvaluationMetrics()) {
         double metricResult = metric.getMetricResult(expectedMergedEntities, mergedEntities);
         evaluationResult.addEvaluationResult(metric.getName(), metricResult);
      }

      writeEvaluationResult(setting, evaluationResult);

      return evaluationResult;
   }


   private void writeEvaluationResult(EvaluationSetting setting, EvaluationResult evaluationResult) {
      String evaluationResultFileName = "evaluation/eval_"
            + new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss").format(Calendar.getInstance().getTime());
      try {
         String evaluationText = getTextualEvaluationResult(setting, evaluationResult);
         LOGGER.info(evaluationText);
         IOUtils.writeToFileCreatingMissingDirectories(new File(evaluationResultFileName), evaluationText);
      } catch (IOException e) {
         LOGGER.error("Could not write evaluation output file.", e);
      }
   }


   @Override
   public String getTextualEvaluationResult(EvaluationSetting setting, EvaluationResult evaluationResult) {
      StringJoiner lineJoiner = new StringJoiner("\n");
      lineJoiner.add("Evaluation: ");
      lineJoiner.add(new SimpleDateFormat("dd-MM-yyyy_HH:mm:ss").format(Calendar.getInstance().getTime()));
      lineJoiner.add("Results: ");
      lineJoiner.add(evaluationResult.toString());
      lineJoiner.add("Setting: ");
      lineJoiner.add(setting.toString());
      return lineJoiner.toString();
   }

}
