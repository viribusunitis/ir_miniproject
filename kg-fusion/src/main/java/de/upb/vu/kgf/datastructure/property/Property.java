package de.upb.vu.kgf.datastructure.property;


import de.upb.vu.kgf.datastructure.IRI;


public interface Property {

   public IRI getType();


   public String getRawValue();


   public String asRDF();
}
