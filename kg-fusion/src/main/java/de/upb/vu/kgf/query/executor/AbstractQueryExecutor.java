package de.upb.vu.kgf.query.executor;


import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Model;


public abstract class AbstractQueryExecutor implements QueryExecutor {

   protected Model datasetModel;

   protected boolean lastQueryClosed;
   private QueryExecution lastQueryExecutionContext;


   public AbstractQueryExecutor(boolean lastQueryClosed) {
      this.lastQueryClosed = lastQueryClosed;
   }


   @Override
   public ResultSet executeQuery(String queryString) {
      lastQueryExecutionContext = QueryExecutionFactory.create(queryString, datasetModel);
      lastQueryClosed = false;
      return lastQueryExecutionContext.execSelect();
   }


   @Override
   public boolean executeAskQuery(String askQueryString) {
      lastQueryExecutionContext = QueryExecutionFactory.create(askQueryString, datasetModel);
      lastQueryClosed = false;
      return lastQueryExecutionContext.execAsk();
   }


   @Override
   public void closeLastQuery() {
      if (!lastQueryClosed) {
         lastQueryExecutionContext.close();
         lastQueryClosed = true;
      }
   }

}
