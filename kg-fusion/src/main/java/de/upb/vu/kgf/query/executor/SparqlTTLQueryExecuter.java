package de.upb.vu.kgf.query.executor;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.apache.jena.rdf.model.ModelFactory;


public class SparqlTTLQueryExecuter extends AbstractQueryExecutor {

   public SparqlTTLQueryExecuter(String ttlDirectoryPath) throws FileNotFoundException {
      super(false);
      datasetModel = ModelFactory.createDefaultModel();
      File directory = new File(ttlDirectoryPath);
      for (File file : directory.listFiles()) {
         InputStream stream = new FileInputStream(file);
         if (file.getName().contains("DS_Store") || file.getName().contains("iris.txt")) {
            // DO NOTHING
         } else if (file.getName().endsWith(".owl")) {
            datasetModel.read(stream, null);
         } else {
            datasetModel.read(stream, null, "NT");
         }
      }
   }


   @Override
   public void shutdown() {
      datasetModel.close();
   }

}
