package de.upb.vu.kgf.evaluation.metric;


import java.util.Random;

import de.upb.vu.kgf.datastructure.Entity;


public class RandomEvaluationMetric extends AbstractAveragingEvaluationMetric {

   @Override
   public double getMetricResult(Entity expectedEntity, Entity computedEntity) {
      return (new Random()).nextDouble();
   }


   @Override
   public String getName() {
      return "random";
   }

}
