package de.upb.vu.kgf.datageneration;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.kgf.Language;
import de.upb.vu.kgf.query.executor.SparqlTDBQueryExecutor;
import de.upb.vu.kgf.util.IOUtils;


public class BenchmarkDataGenerator {

   private static final Logger LOGGER = LoggerFactory.getLogger(BenchmarkDataGenerator.class);

   private static final String REPLACE_TAG = "<TAG>";
   private static final String baseIRI = "http://" + REPLACE_TAG + ".dbpedia.org/resource/";

   private String tdbDirectoryPath;


   public BenchmarkDataGenerator(String tdbDirectoryPath) {
      this.tdbDirectoryPath = tdbDirectoryPath;
   }


   public void generateData(List<String> names, String type) {
      generateData(names, type, Integer.MAX_VALUE);
   }


   public void generateData(List<String> names, String type, int numberOfRequestedResults) {
      StringJoiner outputJoiner = new StringJoiner("\n");
      int numberOfResults = 0;
      for (String name : names) {
         StringJoiner combinedNameStringJoiner = new StringJoiner("  ");
         boolean foundInAllLanguages = true;
         for (Language language : Language.values()) {
            String presumableIRI = getPresumableIRI(name, language);
            if (!doesIRIExist(presumableIRI, language, type)) {
               // outputJoiner.add(presumableIRI + " does not exist.");
               // combinedNameStringJoiner.add("");
               foundInAllLanguages = false;
            } else {
               combinedNameStringJoiner.add(presumableIRI);
            }
         }
         if (foundInAllLanguages) {
            numberOfResults++;
            outputJoiner.add(combinedNameStringJoiner.toString());
            LOGGER.info("Found: " + combinedNameStringJoiner.toString());
         }
         if (numberOfResults >= numberOfRequestedResults) {
            break;
         }
      }
      try {
         IOUtils.writeToFileCreatingMissingDirectories(new File("generated_data/" + type + ".txt"), outputJoiner.toString());
      } catch (IOException e) {
         e.printStackTrace();
      }
   }


   private boolean doesIRIExist(String iri, Language language, String type) {
      SparqlTDBQueryExecutor queryExecuter = new SparqlTDBQueryExecutor(tdbDirectoryPath + "/" + language.getAbbreviation());
      boolean queryResult = queryExecuter.executeAskQuery("PREFIX  rdf:    <http://www.w3.org/1999/02/22-rdf-syntax-ns#> " //
            + "PREFIX   owl:  <http://www.w3.org/2002/07/owl#> " //
            + "PREFIX  dbo:   <http://dbpedia.org/ontology/> " //
            + "ASK WHERE {<" + iri + "> rdf:type dbo:" + type + "}");
      queryExecuter.closeLastQuery();
      queryExecuter.shutdown();
      return queryResult;
   }


   private String getPresumableIRI(String name, Language language) {
      return baseIRI.replaceAll(REPLACE_TAG, language.getAbbreviation()) + name.trim().replaceAll("\\s", "_");
   }
}
