package de.upb.vu.kgf.datastructure.property;


import de.upb.vu.kgf.util.StringUtil;


public class StringProperty extends AbstractProperty {

   private String value;


   public StringProperty(String type, String rawValue, String value) {
      super(type, rawValue);
      this.value = value;
   }


   public String getValue() {
      return value;
   }


   @Override
   public String asRDF() {
      return "<" + type + ">" + StringUtil.SINGLE_WHITESPACE + "\""
            + value.replaceAll(StringUtil.LINE_BREAK, StringUtil.EMPTY_STRING).replaceAll("\"", "'") + "\"";
   }


   @Override
   public String toString() {
      return "\"" + value + "\"";
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + ((value == null) ? 0 : value.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (!super.equals(obj)) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      StringProperty other = (StringProperty) obj;
      if (value == null) {
         if (other.value != null) {
            return false;
         }
      } else if (!value.equals(other.value)) {
         return false;
      }
      return true;
   }


}
