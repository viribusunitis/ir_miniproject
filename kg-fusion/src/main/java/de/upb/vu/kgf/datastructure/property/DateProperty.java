package de.upb.vu.kgf.datastructure.property;


import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import de.upb.vu.kgf.util.StringUtil;


public class DateProperty extends AbstractProperty {

   private LocalDate value;
   private boolean fullDate;


   public DateProperty(String type, String rawValue, LocalDate value, boolean fullDate) {
      super(type, rawValue);
      this.value = value;
      this.fullDate = fullDate;
   }


   public LocalDate getValue() {
      return value;
   }


   @Override
   public String asRDF() {
      if (fullDate) {
         return "<" + type + ">" + StringUtil.SINGLE_WHITESPACE + "\"" + value.format(DateTimeFormatter.ISO_DATE)
               + "\"^^<http://www.w3.org/2001/XMLSchema#date>";
      } else {
         return "<" + type + ">" + StringUtil.SINGLE_WHITESPACE + "\"" + value.getYear() + "\"^^<http://www.w3.org/2001/XMLSchema#gYear>";
      }
   }


   @Override
   public String toString() {
      return value.toString();
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + ((value == null) ? 0 : value.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (!super.equals(obj)) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      DateProperty other = (DateProperty) obj;
      if (value == null) {
         if (other.value != null) {
            return false;
         }
      } else if (!value.equals(other.value)) {
         return false;
      }
      return true;
   }


}
