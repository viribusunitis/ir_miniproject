package de.upb.vu.kgf.fusion.entitymerging;


import java.util.List;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.datastructure.property.IRIProperty;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.util.StringUtil;


public class DefaultEntityMerger implements EntityMerger {

   private static final String HTTP_IRMP_DE = "http://example.com/";


   @Override
   public Entity mergeEntities(Entity referenceEntity, List<Entity> entities) {
      for (Entity entity : entities) {
         referenceEntity.getProperties().addAll(entity.getProperties());
      }
      resetIRIs(referenceEntity);
      return referenceEntity;
   }


   private void resetIRIs(Entity entity) {
      entity.setIri(new IRI(HTTP_IRMP_DE + entity.getLabel().replaceAll(StringUtil.SINGLE_WHITESPACE, StringUtil.UNDERLINE)));
      for (Property property : entity.getProperties()) {
         if (property instanceof IRIProperty) {
            IRIProperty iriProperty = (IRIProperty) property;
            String iriIdentifier = iriProperty.getValue().getIdentifier();
            String[] splittedIriIdentifier = iriIdentifier.split(StringUtil.SLASH);
            if (splittedIriIdentifier[splittedIriIdentifier.length - 2].equals("resource")) {
               IRI newIri = new IRI(HTTP_IRMP_DE + splittedIriIdentifier[splittedIriIdentifier.length - 1]);
               iriProperty.setValue(newIri);
            }
         }
      }
   }

}
