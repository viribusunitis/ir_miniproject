package de.upb.vu.kgf.fusion.entitymerging;


import java.util.List;

import de.upb.vu.kgf.datastructure.Entity;


public interface EntityMerger {

   public Entity mergeEntities(Entity referenceEntity, List<Entity> entities);
}
