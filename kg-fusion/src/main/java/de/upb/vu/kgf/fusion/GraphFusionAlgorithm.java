package de.upb.vu.kgf.fusion;


import java.util.List;

import de.upb.vu.kgf.Language;
import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;


public interface GraphFusionAlgorithm {

   public List<Entity> runFusion(List<IRI> irisOfObjectsToFuse, Language sourceLanguage, List<Language> languagesToFuseWith);


   public Entity runFusion(IRI iriOfObjectToFuse, Language sourceLanguage, List<Language> languagesToFuseWith);
}
