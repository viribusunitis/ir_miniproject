package de.upb.vu.kgf.datastructure;


public class IRI {

   private String identifier;


   public IRI(String identifier) {
      initialize(identifier.trim());
   }


   private void initialize(String identifier) {
      if (isAbbreviatedForm(identifier)) {
         String abbreviation = identifier.split(":", 2)[0];
         this.identifier = identifier.replaceFirst(abbreviation + ":", PrefixStorage.getFullVersionOfPrefix(abbreviation));
      } else {
         this.identifier = identifier;
      }
   }


   private boolean isAbbreviatedForm(String identifier) {
      return identifier.contains(":") && PrefixStorage.containsFullVersionOfPrefix(identifier.split(":", 2)[0]);
   }


   public String getIdentifier() {
      return identifier;
   }


   @Override
   public String toString() {
      return identifier;
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = 1;
      result = prime * result + ((identifier == null) ? 0 : identifier.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (obj == null) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      IRI other = (IRI) obj;
      if (identifier == null) {
         if (other.identifier != null) {
            return false;
         }
      } else if (!identifier.equals(other.identifier)) {
         return false;
      }
      return true;
   }


}
