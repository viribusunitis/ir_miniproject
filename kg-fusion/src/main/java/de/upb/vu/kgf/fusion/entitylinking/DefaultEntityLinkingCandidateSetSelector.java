package de.upb.vu.kgf.fusion.entitylinking;


import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

import org.apache.jena.query.QuerySolution;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.Resource;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.fusion.creation.EntityCreator;
import de.upb.vu.kgf.query.QueryStore;
import de.upb.vu.kgf.query.executor.QueryExecutor;
import de.upb.vu.kgf.query.executor.SparqlTDBQueryExecutor;
import de.upb.vu.kgf.util.NameCleaner;


public class DefaultEntityLinkingCandidateSetSelector implements EntityLinkingCandidateSetSelector {

   private int maximumNumberOfResults;
   private double levenstheinDistanceThreshold;

   private String tdbLocationOfTargetRDFGraph;

   private QueryExecutor queryExecutor;
   private EntityCreator entityCreator;


   public DefaultEntityLinkingCandidateSetSelector() {
      this(Integer.MAX_VALUE, 0.5);
   }


   public DefaultEntityLinkingCandidateSetSelector(int maximumNumberOfResults, double levenstheinDistanceThreshold) {
      this.maximumNumberOfResults = maximumNumberOfResults;
      this.levenstheinDistanceThreshold = levenstheinDistanceThreshold;
   }


   private void initialize(String tdbStorageOfTargetGraph) {
      if (this.tdbLocationOfTargetRDFGraph == null || !this.tdbLocationOfTargetRDFGraph.equals(tdbStorageOfTargetGraph)) {
         this.tdbLocationOfTargetRDFGraph = tdbStorageOfTargetGraph;
         this.queryExecutor = new SparqlTDBQueryExecutor(tdbLocationOfTargetRDFGraph);
         this.entityCreator = new EntityCreator(queryExecutor);
      }
   }


   @Override
   public List<Entity> selectLinkingCandidates(Entity entity, String tdbStorageOfTargetGraph) {
      initialize(tdbStorageOfTargetGraph);
      String queryString = createQueryStringForFindingSimilarEntities(entity);
      ResultSet resultSet = queryExecutor.executeQuery(queryString);
      return createEntityListFromResultSet(resultSet);
   }


   private List<Entity> createEntityListFromResultSet(ResultSet resultSet) {
      List<Entity> foundEntities = new ArrayList<>();
      while (resultSet.hasNext()) {
         QuerySolution solution = resultSet.next();
         Resource resource = solution.getResource("?s");
         String resourceUri = resource.getURI();
         Entity entity = entityCreator.createEntity(new IRI(resourceUri));
         foundEntities.add(entity);
      }
      return foundEntities;
   }


   private String createQueryStringForFindingSimilarEntities(Entity entity) {
      String cleanedEnityName = NameCleaner.cleanName(entity.getLabel());

      String rawQueryString = QueryStore.FETCH_ALL_ENTITIES_WITH_SIMILAR_NAME.getQueryString();
      rawQueryString = rawQueryString.replaceAll("\\$TYPE\\$", "<" + entity.getType().getIdentifier() + ">");
      rawQueryString = rawQueryString.replaceAll("\\$LIMIT\\$", String.valueOf(maximumNumberOfResults));
      rawQueryString = rawQueryString.replaceAll("\\$FULLNAME\\$", cleanedEnityName);
      rawQueryString = rawQueryString.replaceAll("\\$THRESHOLD\\$", String.valueOf(levenstheinDistanceThreshold));

      StringJoiner labelCriteriaJoiner = new StringJoiner(" || \n");
      String[] splitEntityLabel = cleanedEnityName.split("\\s+");
      for (String labelPart : splitEntityLabel) {
         labelCriteriaJoiner.add("regex(str(?s), \"" + labelPart + "\")");
      }
      return rawQueryString.replaceAll("\\$CRITERIA\\$", labelCriteriaJoiner.toString());
   }

}
