package de.upb.vu.kgf.evaluation;


import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;


public class EvaluationResult {
   private Map<String, Double> metricResults;


   public EvaluationResult() {
      this.metricResults = new HashMap<>();
   }


   public void addEvaluationResult(String metricName, double metricResult) {
      metricResults.put(metricName, metricResult);
   }


   public double getMetricResult(String metricName) {
      return metricResults.get(metricName);
   }


   @Override
   public String toString() {
      StringJoiner lineJoiner = new StringJoiner("\n");
      for (String key : metricResults.keySet()) {
         lineJoiner.add(key + " := " + metricResults.get(key));
      }
      return lineJoiner.toString();
   }


}
