package de.upb.vu.kgf.evaluation.metric.ir;


import java.util.HashSet;
import java.util.Set;

import com.google.common.collect.Sets;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.evaluation.metric.AbstractAveragingEvaluationMetric;


public class IRPrecisionEvaluationMetric extends AbstractAveragingEvaluationMetric {

   @Override
   public double getMetricResult(Entity expectedEntity, Entity computedEntity) {
      Set<Property> relevantProperties = new HashSet<>(expectedEntity.getProperties());
      Set<Property> retrievedProperties = new HashSet<>(computedEntity.getProperties());
      double precision = (Sets.intersection(relevantProperties, retrievedProperties).size()) / (double) (retrievedProperties.size());
      return precision;
   }


   @Override
   public String getName() {
      return "ir-precision";
   }

}
