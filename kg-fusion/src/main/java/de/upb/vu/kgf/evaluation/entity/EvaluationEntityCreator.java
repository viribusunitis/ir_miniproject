package de.upb.vu.kgf.evaluation.entity;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.EntityTriple;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.datastructure.IRITriple;
import de.upb.vu.kgf.datastructure.property.IRIProperty;
import de.upb.vu.kgf.datastructure.property.Property;
import de.upb.vu.kgf.fusion.creation.EntityCreator;
import de.upb.vu.kgf.query.executor.SparqlTDBQueryExecutor;
import de.upb.vu.kgf.util.IOUtils;
import de.upb.vu.kgf.util.StringUtil;


public class EvaluationEntityCreator {


   private final static String TDB_DIRECTORY_PATH = "/Users/Tanja/Development/Uni/workspace_ir_miniproject/tdb_storage/";
   private static final String HTTP_IRMP_DE = "http://example.com/";

   private static List<Entity> mergedEntities;
   private static List<String> iris;


   public static void create(String folder) throws IOException {
      List<IRITriple> iriTriples = readEvaluationPersons(folder);
      List<EntityTriple> entityTriples = createEntityTriples(iriTriples);
      mergeEntities(entityTriples);
   }


   private static List<IRITriple> readEvaluationPersons(String folder) throws IOException {
      String[] entities = IOUtils.readStringFromFile(folder + "persons.txt").split(StringUtil.LINE_BREAK);
      List<IRITriple> iriTriples = new ArrayList<>();
      for (int i = 0; i < entities.length; i++) {
         iriTriples.add(new IRITriple(entities[i].split("\\s+")));
      }
      return iriTriples;
   }


   private static List<EntityTriple> createEntityTriples(List<IRITriple> iriTriples) {
      List<EntityTriple> entityTriples = new ArrayList<>();
      EntityCreator entityCreatorDe = new EntityCreator(new SparqlTDBQueryExecutor(TDB_DIRECTORY_PATH + "de"));
      EntityCreator entityCreatorNl = new EntityCreator(new SparqlTDBQueryExecutor(TDB_DIRECTORY_PATH + "nl"));
      EntityCreator entityCreatorPt = new EntityCreator(new SparqlTDBQueryExecutor(TDB_DIRECTORY_PATH + "pt"));
      for (IRITriple iriTriple : iriTriples) {
         Entity de = entityCreatorDe.createEntity(iriTriple.getDe());
         Entity nl = entityCreatorNl.createEntity(iriTriple.getNl());
         Entity pt = entityCreatorPt.createEntity(iriTriple.getPt());
         entityTriples.add(new EntityTriple(de, nl, pt));
      }
      return entityTriples;
   }


   private static void mergeEntities(List<EntityTriple> entityTriples) {
      mergedEntities = new ArrayList<>();
      iris = new ArrayList<>();
      for (EntityTriple entityTriple : entityTriples) {
         IRI iri = new IRI(HTTP_IRMP_DE + entityTriple.getDe().getLabel().replaceAll(StringUtil.SINGLE_WHITESPACE, StringUtil.UNDERLINE));
         iris.add(iri.getIdentifier());

         Set<Property> mergedProperties = new HashSet<>();
         mergedProperties.addAll(resetIRIs(entityTriple.getDe().getProperties()));
         mergedProperties.addAll(resetIRIs(entityTriple.getNl().getProperties()));
         mergedProperties.addAll(resetIRIs(entityTriple.getPt().getProperties()));

         Entity newEntity = new Entity(iri, new ArrayList<>(mergedProperties));
         mergedEntities.add(newEntity);
      }
   }


   private static Set<Property> resetIRIs(Set<Property> properties) {
      for (Property property : properties) {
         if (property instanceof IRIProperty) {
            IRIProperty iriProperty = (IRIProperty) property;
            String iriIdentifier = iriProperty.getValue().getIdentifier();
            String[] splittedIriIdentifier = iriIdentifier.split(StringUtil.SLASH);
            if (splittedIriIdentifier[splittedIriIdentifier.length - 2].equals("resource")) {
               IRI newIri = new IRI(HTTP_IRMP_DE + splittedIriIdentifier[splittedIriIdentifier.length - 1]);
               iriProperty.setValue(newIri);
            }
         }
      }
      return properties;
   }


   public static List<Entity> getMergedEntities() {
      return mergedEntities;
   }


   public static List<String> getIRIs() {
      return iris;
   }

}
