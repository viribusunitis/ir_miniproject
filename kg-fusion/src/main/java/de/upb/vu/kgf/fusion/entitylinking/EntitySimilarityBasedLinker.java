package de.upb.vu.kgf.fusion.entitylinking;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.fusion.entitylinking.similarity.EntitySimilarityMeasure;


public class EntitySimilarityBasedLinker implements EntityLinker {

   private static final Logger LOGGER = LoggerFactory.getLogger(EntitySimilarityBasedLinker.class);

   public List<EntitySimilarityMeasure> similarityMeasures;


   public EntitySimilarityBasedLinker(List<EntitySimilarityMeasure> similarityMeasures) {
      this.similarityMeasures = similarityMeasures;
   }


   @Override
   public Entity selectFinalCandidateForLinking(Entity entity, List<Entity> candidates) {
      Entity bestCandidate = null;
      double bestSimilarity = 0;

      for (Entity candidate : candidates) {
         double averageSimilarity = 0;
         for (EntitySimilarityMeasure similarityMeasure : similarityMeasures) {
            averageSimilarity += similarityMeasure.computeEntitySimilarity(entity, candidate);
         }
         averageSimilarity = averageSimilarity / similarityMeasures.size();
         if (averageSimilarity > bestSimilarity) {
            bestSimilarity = averageSimilarity;
            bestCandidate = candidate;
         }
      }
      IRI bestCandidateIRI = bestCandidate != null ? bestCandidate.getIRI() : null;
      LOGGER.info("Linked {} to {}", entity.getIRI(), bestCandidateIRI);
      return bestCandidate;
   }

}
