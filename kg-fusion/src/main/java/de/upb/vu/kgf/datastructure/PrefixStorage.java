package de.upb.vu.kgf.datastructure;


import java.util.HashMap;
import java.util.List;

import de.upb.vu.kgf.query.QueryUtil;


public class PrefixStorage {
   private static HashMap<String, String> prefixMap;


   public static void initialize() {
      if (prefixMap == null) {
         prefixMap = new HashMap<>();
         List<String> rawPrefixes = QueryUtil.getPrefixes();
         for (String prefix : rawPrefixes) {
            String cleanedPrefix = prefix.replaceAll("\\s+", " ").replaceAll("PREFIX", "").trim();
            String[] splitPrefix = cleanedPrefix.split(":", 2);
            if (splitPrefix.length != 2) {
               throw new RuntimeException("The prefix " + prefix + " is in a wrong format.");
            }
            String abbreviation = splitPrefix[0];
            String longVersion = splitPrefix[1].trim().substring(1, splitPrefix[1].length() - 2);
            prefixMap.put(abbreviation, longVersion);
         }
      }
   }


   public static String getFullVersionOfPrefix(String prefix) {
      if (prefixMap == null) {
         initialize();
      }
      return prefixMap.get(prefix);
   }


   public static boolean containsFullVersionOfPrefix(String prefix) {
      if (prefixMap == null) {
         initialize();
      }
      return prefixMap.containsKey(prefix);
   }
}
