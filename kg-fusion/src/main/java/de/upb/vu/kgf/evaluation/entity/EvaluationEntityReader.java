package de.upb.vu.kgf.evaluation.entity;


import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.fusion.creation.EntityCreator;
import de.upb.vu.kgf.query.executor.SparqlTTLQueryExecuter;
import de.upb.vu.kgf.util.IOUtils;
import de.upb.vu.kgf.util.StringUtil;


public class EvaluationEntityReader {

   public static List<Entity> read(String folder) throws IOException {
      List<IRI> iris = readIRIs(folder + "iris.txt");
      List<Entity> entities = new ArrayList<>();
      EntityCreator entityCreator = new EntityCreator(new SparqlTTLQueryExecuter(folder));
      for (IRI iri : iris) {
         entities.add(entityCreator.createEntity(iri));
      }
      return entities;
   }


   public static List<IRI> readIRIs(String fileName) throws IOException {
      List<IRI> iris = new ArrayList<>();
      String fileContent = IOUtils.readStringFromFile(fileName);
      for (String iri : Arrays.asList(fileContent.split(StringUtil.LINE_BREAK))) {
         iris.add(new IRI(iri.split("\\s+")[0]));
      }
      return iris;
   }

}
