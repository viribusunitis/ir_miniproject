package de.upb.vu.kgf.query.tdb;


import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.compress.compressors.CompressorException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.kgf.Language;


public class RunTDBStorageCreation {

   private static final Logger LOGGER = LoggerFactory.getLogger(RunTDBStorageCreation.class);

   private static String tdbStorageDirectoryPath;

   private static String dbpediaDumpLocation;


   public static void main(String[] args) throws FileNotFoundException, CompressorException {
      if (args.length != 2) {
         LOGGER.error("Requiring 2 command line arguments: \n1) path to desired TDB storage \n2) path to dbpedia dump");
         return;
      }
      tdbStorageDirectoryPath = args[0];
      dbpediaDumpLocation = args[1];

      LOGGER.info("Starting TDB storage creation at location \"{}\" from dump \"{}\"", tdbStorageDirectoryPath, dbpediaDumpLocation);

      for (Language language : Language.values()) {
         String fullStorageDirectory = tdbStorageDirectoryPath + "/" + language.getAbbreviation();
         String fullDumpDirectory = dbpediaDumpLocation + "/" + language.getAbbreviation();

         LOGGER.info("Starting TDB storage creation for {} at location \"{}\" from dump \"{}\"", language.toString(),
               tdbStorageDirectoryPath, dbpediaDumpLocation);

         List<String> dumpFilePaths = collectFilePathsInDirectory(fullDumpDirectory);
         dumpFilePaths.add(0, dbpediaDumpLocation + "/dbpedia_2016-10.owl");
         TDBStorageCreator tdbStorageCreator = new TDBStorageCreator(fullStorageDirectory, dumpFilePaths);
         tdbStorageCreator.createTDBStore();
      }
   }


   private static List<String> collectFilePathsInDirectory(String directoryPath) {
      List<String> files = new ArrayList<>();
      File directory = new File(directoryPath);
      for (File file : directory.listFiles()) {
         if (!file.getName().contains("DS_Store")) {
            files.add(file.getAbsolutePath());
         }
      }
      return files;
   }

}
