package de.upb.vu.kgf.nlp;


public class LevenstheinDistanceComputer {

   public static double computeDistance(String s1, String s2) {
      char[] s1Chars = s1.toCharArray();
      char[] s2Chars = s2.toCharArray();
      int[][] matrix = new int[s1.length() + 1][s2.length() + 1];
      for (int i = 0; i <= s1.length(); i++) {
         matrix[i][0] = i;
      }
      for (int j = 0; j <= s2.length(); j++) {
         matrix[0][j] = j;
      }
      for (int i = 1; i <= s1.length(); i++) {
         for (int j = 1; j <= s2.length(); j++) {
            if (s1Chars[i - 1] == s2Chars[j - 1]) {
               matrix[i][j] = Math.min(matrix[i - 1][j] + 1, Math.min(matrix[i][j - 1] + 1, matrix[i - 1][j - 1]));
            } else {
               matrix[i][j] = Math.min(matrix[i - 1][j] + 1, Math.min(matrix[i][j - 1] + 1, matrix[i - 1][j - 1] + 1));
            }
         }
      }
      return matrix[s1.length()][s2.length()] / (double) Math.max(s1.length(), s2.length());
   }
}
