package de.upb.vu.kgf.query;


public enum QueryStore {
   FETCH_ALL_PROPERTIES_OF_IRI("queries/fetch_all_properties_of_iri.txt"),
   FETCH_ALL_ENTITIES_WITH_SIMILAR_NAME("queries/fetch_all_entities_with_similar_name.txt"),
   FETCH_ALL_PERSON_PROPERTIES("queries/fetch_all_person_properties.txt");

   private String pathToQueryFile;


   private QueryStore(String pathToQueryFile) {
      this.pathToQueryFile = pathToQueryFile;
   }


   public String getQueryString() {
      return QueryUtil.getQueryString(pathToQueryFile);
   }
}
