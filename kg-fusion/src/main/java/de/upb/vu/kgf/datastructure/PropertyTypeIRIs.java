package de.upb.vu.kgf.datastructure;


public class PropertyTypeIRIs {

   public static final IRI ENTITY_TYPE = new IRI("http://www.w3.org/1999/02/22-rdf-syntax-ns#type");
   public static final IRI LABEL = new IRI("http://www.w3.org/2000/01/rdf-schema#label");
}
