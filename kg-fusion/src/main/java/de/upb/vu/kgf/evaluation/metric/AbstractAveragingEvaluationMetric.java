package de.upb.vu.kgf.evaluation.metric;


import java.util.List;

import de.upb.vu.kgf.datastructure.Entity;


public abstract class AbstractAveragingEvaluationMetric implements EvaluationMetric {

   @Override
   public double getMetricResult(List<Entity> expectedEntities, List<Entity> computedEntities) {
      if (expectedEntities.size() != computedEntities.size()) {
         throw new RuntimeException("Entity lists have different sizes: " + expectedEntities.size() + " != " + computedEntities.size());
      }
      double resultSum = 0;
      for (int i = 0; i < expectedEntities.size(); i++) {
         resultSum += getMetricResult(expectedEntities.get(i), computedEntities.get(i));
      }
      return resultSum / expectedEntities.size();
   }


   @Override
   public String toString() {
      return getName();
   }


}
