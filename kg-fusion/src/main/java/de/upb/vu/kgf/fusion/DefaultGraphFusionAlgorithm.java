package de.upb.vu.kgf.fusion;


import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.upb.vu.kgf.Language;
import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.datastructure.IRI;
import de.upb.vu.kgf.fusion.creation.EntityCreator;
import de.upb.vu.kgf.fusion.entitylinking.EntityLinker;
import de.upb.vu.kgf.fusion.entitylinking.EntityLinkingCandidateSetSelector;
import de.upb.vu.kgf.fusion.entitymerging.EntityMerger;
import de.upb.vu.kgf.fusion.entitymerging.EntityMergingPostProcessor;
import de.upb.vu.kgf.query.executor.SparqlTDBQueryExecutor;
import de.upb.vu.kgf.util.TDBUtil;


public class DefaultGraphFusionAlgorithm implements GraphFusionAlgorithm {

   private static final Logger LOGGER = LoggerFactory.getLogger(DefaultGraphFusionAlgorithm.class);

   private EntityLinkingCandidateSetSelector entityLinkingCandidateSetSelector;
   private EntityLinker entityLinker;
   private EntityMerger entityMerger;
   private EntityMergingPostProcessor entityMergingPostProcessor;
   private String tdbStorageDirectory;


   public DefaultGraphFusionAlgorithm(String tdbStorageDirectory, EntityLinkingCandidateSetSelector entityLinkingCandidateSetSelector,
         EntityLinker entityLinker, EntityMerger entityMerger, EntityMergingPostProcessor entityMergingPostProcessor) {
      super();
      this.entityLinkingCandidateSetSelector = entityLinkingCandidateSetSelector;
      this.entityLinker = entityLinker;
      this.entityMerger = entityMerger;
      this.entityMergingPostProcessor = entityMergingPostProcessor;
      this.tdbStorageDirectory = tdbStorageDirectory;
   }


   public DefaultGraphFusionAlgorithm(String tdbStorageDirectory, EntityLinkingCandidateSetSelector entityLinkingCandidateSetSelector,
         EntityLinker entityLinker, EntityMerger entityMerger) {
      this(tdbStorageDirectory, entityLinkingCandidateSetSelector, entityLinker, entityMerger, null);
   }


   @Override
   public List<Entity> runFusion(List<IRI> irisOfObjectsToFuse, Language sourceLanguage, List<Language> languagesToFuseWith) {
      List<Entity> mergedEntities = new ArrayList<>();
      for (IRI iriOfObjectToFuse : irisOfObjectsToFuse) {
         LOGGER.info("Merging {}.", iriOfObjectToFuse);
         Entity mergedEntity = runFusion(iriOfObjectToFuse, sourceLanguage, languagesToFuseWith);
         mergedEntities.add(mergedEntity);
      }
      return mergedEntities;
   }


   @Override
   public Entity runFusion(IRI iriOfObjectToFuse, Language sourceLanguage, List<Language> languagesToFuseWith) {
      String sourceLanguageTdbDirectory = TDBUtil.getCompleteTDBDirectory(tdbStorageDirectory, sourceLanguage);
      EntityCreator sourceLanguageEntityCreator = new EntityCreator(new SparqlTDBQueryExecutor(sourceLanguageTdbDirectory));

      Entity sourceLanguageEntity = sourceLanguageEntityCreator.createEntity(iriOfObjectToFuse);
      List<Entity> entitiesToMergeWith = new ArrayList<>();
      for (Language language : languagesToFuseWith) {

         String targetLanguageTdbDirectory = TDBUtil.getCompleteTDBDirectory(tdbStorageDirectory, language);
         List<Entity> linkingCandidates = entityLinkingCandidateSetSelector.selectLinkingCandidates(sourceLanguageEntity,
               targetLanguageTdbDirectory);

         Entity entityToLinkWith = entityLinker.selectFinalCandidateForLinking(sourceLanguageEntity, linkingCandidates);
         if (entityToLinkWith != null) {
            entitiesToMergeWith.add(entityToLinkWith);
         }
      }
      Entity mergedEntity = entityMerger.mergeEntities(sourceLanguageEntity, entitiesToMergeWith);
      if (entityMergingPostProcessor != null) {
         return entityMergingPostProcessor.postProcess(mergedEntity);
      }
      return mergedEntity;
   }


}
