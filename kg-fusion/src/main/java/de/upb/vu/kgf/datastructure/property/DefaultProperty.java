package de.upb.vu.kgf.datastructure.property;


public class DefaultProperty extends AbstractProperty {


   public DefaultProperty(String type, String rawValue) {
      super(type, rawValue);
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + ((rawValue == null) ? 0 : rawValue.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (!super.equals(obj)) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      DefaultProperty other = (DefaultProperty) obj;
      if (rawValue == null) {
         if (other.rawValue != null) {
            return false;
         }
      } else if (!rawValue.equals(other.rawValue)) {
         return false;
      }
      return true;
   }


}
