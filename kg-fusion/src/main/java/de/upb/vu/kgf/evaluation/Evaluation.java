package de.upb.vu.kgf.evaluation;


public interface Evaluation {

   public EvaluationResult evaluate(EvaluationSetting setting);


   public String getTextualEvaluationResult(EvaluationSetting setting, EvaluationResult evaluationResult);
}
