package de.upb.vu.kgf.datastructure;


public class EntityTriple {

   private Entity de;
   private Entity nl;
   private Entity pt;


   public EntityTriple(Entity de, Entity nl, Entity pt) {
      super();
      this.de = de;
      this.nl = nl;
      this.pt = pt;
   }


   public Entity getDe() {
      return de;
   }


   public Entity getNl() {
      return nl;
   }


   public Entity getPt() {
      return pt;
   }

}
