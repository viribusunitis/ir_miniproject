package de.upb.vu.kgf.fusion.entitylinking.similarity;


import de.upb.vu.kgf.datastructure.Entity;


public interface EntitySimilarityMeasure {

   public double computeEntitySimilarity(Entity entity1, Entity entity2);
}
