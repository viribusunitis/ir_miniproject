package de.upb.vu.kgf.fusion;


import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.StringJoiner;

import de.upb.vu.kgf.datastructure.Entity;
import de.upb.vu.kgf.util.IOUtils;
import de.upb.vu.kgf.util.StringUtil;


public class EntityWriter {

   private static final String TTL = ".ttl";
   private static final String IRIS = "iris.txt";


   public static void writeEntitiesToFile(String folder, List<Entity> entities) throws IOException {
      for (Entity entity : entities) {
         String entityAsRDF = entity.asRDF();
         String fileName = entity.getLabel().replaceAll(StringUtil.SINGLE_WHITESPACE, StringUtil.UNDERLINE);
         IOUtils.writeToFileCreatingMissingDirectories(new File(folder + "entity/" + fileName + TTL), entityAsRDF);
      }
   }


   public static void writeIRIsToFile(String folder, List<String> iris) throws IOException {
      StringJoiner joiner = new StringJoiner(StringUtil.LINE_BREAK);
      for (String iri : iris) {
         joiner.add(iri);
      }
      IOUtils.writeToFileCreatingMissingDirectories(new File(folder + "entity/" + IRIS), joiner.toString());
   }

}
