package de.upb.vu.kgf.fusion.entitylinking;


import java.util.List;

import de.upb.vu.kgf.datastructure.Entity;


public interface EntityLinker {

   public Entity selectFinalCandidateForLinking(Entity entity, List<Entity> candidates);
}
