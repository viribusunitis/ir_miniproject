package de.upb.vu.kgf.fusion.entitylinking;


import java.util.List;
import java.util.Random;

import de.upb.vu.kgf.datastructure.Entity;


public class RandomEntityLinker implements EntityLinker {

   @Override
   public Entity selectFinalCandidateForLinking(Entity entity, List<Entity> candidates) {
      return candidates.get(new Random().nextInt(candidates.size()));
   }

}
