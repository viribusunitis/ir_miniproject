package de.upb.vu.kgf.datastructure.property;


import de.upb.vu.kgf.util.StringUtil;


public class NumericalProperty extends AbstractProperty {

   private Number value;


   public NumericalProperty(String type, String rawValue, Number value) {
      super(type, rawValue);
      this.value = value;
   }


   public Number getValue() {
      return value;
   }


   @Override
   public String asRDF() {
      if (value instanceof Integer) {
         return "<" + type + ">" + StringUtil.SINGLE_WHITESPACE + "\"" + value.toString()
               + "\"^^<http://www.w3.org/2001/XMLSchema#integer>";
      } else {
         return "<" + type + ">" + StringUtil.SINGLE_WHITESPACE + "\"" + value.toString() + "\"^^<http://www.w3.org/2001/XMLSchema#double>";
      }
   }


   @Override
   public String toString() {
      return value.toString();
   }


   @Override
   public int hashCode() {
      final int prime = 31;
      int result = super.hashCode();
      result = prime * result + ((value == null) ? 0 : value.hashCode());
      return result;
   }


   @Override
   public boolean equals(Object obj) {
      if (this == obj) {
         return true;
      }
      if (!super.equals(obj)) {
         return false;
      }
      if (getClass() != obj.getClass()) {
         return false;
      }
      NumericalProperty other = (NumericalProperty) obj;
      if (value == null) {
         if (other.value != null) {
            return false;
         }
      } else if (!value.toString().equals(other.value.toString())) {
         return false;
      }
      return true;
   }


}
